package com.mehdi.produit.produit.responses;

import com.mehdi.produit.produit.entities.Category;
import lombok.Data;


import java.util.Date;
@Data
public class ProductResponse {
    private Long id;
    private String name;
    private String description;
    private String image;
    private String slug;
    private Long categoryId;
    private Date dateCreat;
    private Date dateUpdate;
    private Boolean deleting;
}
