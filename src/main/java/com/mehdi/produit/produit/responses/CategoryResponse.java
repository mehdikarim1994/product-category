package com.mehdi.produit.produit.responses;

import lombok.Data;

import java.util.Date;

@Data
public class CategoryResponse {

    private Long id;
    private String name;
    private String description;
    private String slug;
    private Boolean deleting;
    private Date dateCreat;
    private Date dateUpdate;
}
