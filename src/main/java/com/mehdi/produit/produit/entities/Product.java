package com.mehdi.produit.produit.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Product implements Serializable {
    @Serial
    private static final long serialVersionUID = 4128094462604644752L;
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id

    private Long id;
    private String name;
    private String description;
    private String image;
    private String slug;
    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;
    private Date dateCreat;
    private Date dateUpdate;
    private Boolean deleting;

}
