package com.mehdi.produit.produit.repositories;

import com.mehdi.produit.produit.entities.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long> {
    Page<Product> findByDeletingFalse(Pageable pageable);

}
