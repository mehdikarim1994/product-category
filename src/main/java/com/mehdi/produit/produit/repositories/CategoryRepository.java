package com.mehdi.produit.produit.repositories;

import com.mehdi.produit.produit.entities.Category;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Map;

public interface CategoryRepository extends JpaRepository<Category, Long> {
    Category findByName(String name);
    Page<Category> findByDeletingFalse(Pageable pageable);

}
