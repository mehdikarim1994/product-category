package com.mehdi.produit.produit.services;

import com.mehdi.produit.produit.dto.ProductDto;
import com.mehdi.produit.produit.entities.Category;
import com.mehdi.produit.produit.entities.Product;

import java.util.List;

public interface ProductService {
    List<ProductDto> getProducts(int page, int limit);

    ProductDto getProduct(Long id);

    ProductDto saveProduct(ProductDto productDto);

    ProductDto updateProduct(Long id, ProductDto productDto);

    void deleteProduct(Long id);
}
