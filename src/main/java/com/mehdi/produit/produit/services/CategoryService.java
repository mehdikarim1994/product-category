package com.mehdi.produit.produit.services;

import com.mehdi.produit.produit.dto.CategoryDto;
import com.mehdi.produit.produit.entities.Category;

import java.util.List;

public interface CategoryService {
    CategoryDto createCategory(CategoryDto categoryDto);
    List<CategoryDto> allCategories(int page, int limit);
    CategoryDto getCategory(String name) throws Exception;

    CategoryDto getCategoryById(Long id) throws Exception;
    CategoryDto updateCategory(Long id, CategoryDto categoryDto) throws Exception;

    void DeleteCategory(Long id) throws Exception;

}
