package com.mehdi.produit.produit.services;

import com.mehdi.produit.produit.dto.CategoryDto;
import org.modelmapper.ModelMapper;
import com.mehdi.produit.produit.entities.Category;
import com.mehdi.produit.produit.repositories.CategoryRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private ModelMapper mapper;

    @Override
    public CategoryDto createCategory(CategoryDto categoryDto) {


        Category category = mapper.map(categoryDto, Category.class);

        category.setDateCreat(new Date());
        category.setDateUpdate(new Date());
        category.setDeleting(false);

        Category newCategory = categoryRepository.save(category);

        return mapper.map(newCategory, CategoryDto.class);

    }

    @Override
    public List<CategoryDto> allCategories(int page, int limit) {
        if (page > 0) {
            page -= 1;
        }

        List<CategoryDto> categoryDtos = new ArrayList<>();
        PageRequest pageable = PageRequest.of(page, limit);

        Page<Category> categoryPage = categoryRepository.findByDeletingFalse(pageable);
        List<Category> categories = categoryPage.getContent();

        for (Category category : categories) {
            CategoryDto categoryDto = new CategoryDto();
            BeanUtils.copyProperties(category, categoryDto);
            categoryDtos.add(categoryDto);
        }

        return categoryDtos;

    }


    @Override
    public CategoryDto getCategory(String name) throws Exception {
        Category category = categoryRepository.findByName(name);

        if (category == null)
            throw new Exception(name);

        CategoryDto categoryDto = new CategoryDto();
        BeanUtils.copyProperties(category, categoryDto);
        return categoryDto;
    }



    @Override
    public CategoryDto getCategoryById(Long id) throws Exception {
        Optional<Category> category = categoryRepository.findById(id);

        if (category.isEmpty()) {
            throw new Exception("Category not found");
        }

        CategoryDto categoryDto = new CategoryDto();
        BeanUtils.copyProperties(category.get(), categoryDto);

        return categoryDto;
    }

    @Override
    public CategoryDto updateCategory(Long id, CategoryDto categoryDto) throws Exception {

        Optional<Category> optCategory = categoryRepository.findById(id);
        if (optCategory.isEmpty()) {
            throw new Exception("Category not found");
        }
        Category category = optCategory.get();
        category.setName(categoryDto.getName());
        category.setDescription(categoryDto.getDescription());
        category.setSlug(categoryDto.getSlug());

//        Todo : Fix date of creation should be fix when updating !
        category.setDeleting(categoryDto.getDeleting() != null && categoryDto.getDeleting());
        category.setDateCreat(categoryDto.getDateCreat() == null ?
                new Date() : categoryDto.getDateCreat());

        category.setDateUpdate(new Date());

        Category updatedCategory = categoryRepository.save(category);
        CategoryDto updatedCategoryDto = new CategoryDto();
        BeanUtils.copyProperties(updatedCategory, updatedCategoryDto);
        return updatedCategoryDto;
    }


    @Override
    public void DeleteCategory(Long id) throws Exception {

        Category category = categoryRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Category with id " + id + " not found"));

        category.setDeleting(true);
        category.setDateUpdate(new Date());

        Category updatedCategory = categoryRepository.save(category);
        mapper.map(updatedCategory, CategoryDto.class);



    }
}
