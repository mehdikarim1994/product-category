package com.mehdi.produit.produit.services;

import com.mehdi.produit.produit.dto.ProductDto;
import com.mehdi.produit.produit.entities.Category;
import com.mehdi.produit.produit.entities.Product;
import com.mehdi.produit.produit.repositories.CategoryRepository;
import com.mehdi.produit.produit.repositories.ProductRepository;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private ModelMapper mapper;



    public  ProductServiceImpl(ProductRepository productRepository){
        this.productRepository = productRepository;
    }
    // Get all
    @Override
    public List<ProductDto> getProducts(int page, int limit) {
        if (page > 0) {
            page -= 1;
        }

        List<ProductDto> productDtoo = new ArrayList<>();
        PageRequest pageable = PageRequest.of(page, limit);
        Page<Product> products = productRepository.findByDeletingFalse(pageable);
        for (Product product : products) {
            ProductDto productDto = mapper.map(product, ProductDto.class);
            Optional<Category> category = categoryRepository.findById(product.getCategory().getId());
            category.ifPresent(value -> productDto.setCategoryId(value.getId()));
            productDtoo.add(productDto);
        }
        return productDtoo;
    }

    // get by id
    @Override
    public ProductDto getProduct(Long id) {
        Product product = productRepository.findById(id).orElse(null);
        if (product == null) {
            return null;
        }
        return mapper.map(product, ProductDto.class);
    }

    //create product
    @Override
    public ProductDto saveProduct(ProductDto productDto) {

        Product product = mapper.map(productDto, Product.class);

        product.setDateCreat(new Date());
        product.setDateUpdate(new Date());
        product.setDeleting(false);

        Category category = categoryRepository.getReferenceById(productDto.getCategoryId());
        product.setCategory(category);

        Product newProduct = productRepository.save(product);

        return mapper.map(newProduct, ProductDto.class);
    }


    @Override
    @Transactional

    public ProductDto updateProduct(Long id,ProductDto productDto) {
        Product product = productRepository.findById(productDto.getId())
                .orElseThrow(() -> new EntityNotFoundException("Product with id " + productDto.getId() + " not found"));

        Category category = categoryRepository.findById(productDto.getCategoryId())
                .orElseThrow(() -> new EntityNotFoundException("Category with id " +productDto.getCategoryId() + " not found"));

        product.setName(productDto.getName());
        product.setDescription(productDto.getDescription());
        product.setCategory(category);
        product.setDateUpdate(new Date());
        if(product.getDateCreat() == null){
            product.setDeleting(false);
            product.setDateCreat(product.getDateUpdate());
        }

        Product updatedProduct = productRepository.save(product);
        return mapper.map(updatedProduct, ProductDto.class);
    }

    @Override
    public void deleteProduct(Long id) {
        Product product = productRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Product with id " + id + " not found"));

        product.setDeleting(true);
        product.setDateUpdate(new Date());

        Product deletedProduct = productRepository.save(product);
        mapper.map(deletedProduct, ProductDto.class);

    }
}
