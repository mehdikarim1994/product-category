package com.mehdi.produit.produit.dto;

import com.mehdi.produit.produit.entities.Category;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductDto implements Serializable {
    @Serial
    private static final long serialVersionUID = -1672370666251485872L;
    private Long id;
    private String name;
    private String description;
    private String image;
    private String slug;
    private Long categoryId;
    private Date dateCreat;
    private Date dateUpdate;
    private Boolean deleting;
}
