package com.mehdi.produit.produit.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serial;
import java.io.Serializable;
import java.util.Date;

@NoArgsConstructor
@Data
@AllArgsConstructor
@Builder
public class CategoryDto implements Serializable {

    @Serial
    private static final long serialVersionUID = -1219791756953081485L;
    private Long id;
    private String name;
    private String description;
    private String slug;
    private Date dateCreat;
    private Date dateUpdate;
    private Boolean deleting;

}
