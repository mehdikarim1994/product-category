package com.mehdi.produit.produit.controllers;

import com.mehdi.produit.produit.dto.CategoryDto;
import com.mehdi.produit.produit.requests.CategoryRequest;
import com.mehdi.produit.produit.responses.CategoryResponse;
import com.mehdi.produit.produit.services.CategoryService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin("*")

@RequestMapping("/categories")
public class CategoryController {
    @Autowired
    private CategoryService categoryService;
    @Autowired
    private ModelMapper mapper;

    @PostMapping()
    public ResponseEntity<CategoryResponse> CreateCategory(@RequestBody CategoryRequest categoryRequest) {

        CategoryDto categoryDto =new CategoryDto();
        BeanUtils.copyProperties(categoryRequest, categoryDto);

        // PRESENTATION LAYER
        CategoryDto createCategory = categoryService.createCategory(categoryDto);
        CategoryResponse categoryResponse = new CategoryResponse();

        BeanUtils.copyProperties(createCategory, categoryResponse);
        return new ResponseEntity<>(categoryResponse, HttpStatus.CREATED);
    }



    @GetMapping("/{id}")
    public ResponseEntity<CategoryResponse> getCategory(@PathVariable Long id) throws Exception {
        CategoryDto categoryDto = categoryService.getCategoryById(id);
        CategoryResponse categoryResponse = new CategoryResponse();
        BeanUtils.copyProperties(categoryDto, categoryResponse);
        return new ResponseEntity<CategoryResponse>(categoryResponse, HttpStatus.OK);
    }



    @GetMapping()
    public List<CategoryResponse> allCategories(@RequestParam(value="page", defaultValue = "1") int page,
                                                @RequestParam(value="limit", defaultValue = "7") int limit){

        List<CategoryResponse> categoryResponses = new ArrayList<>();

        List<CategoryDto> categories = categoryService.allCategories(page, limit);

        for(CategoryDto categoryDto: categories){

            CategoryResponse categoryResp = new CategoryResponse();
            BeanUtils.copyProperties(categoryDto, categoryResp);

            categoryResponses.add(categoryResp);
        }
        return categoryResponses;
    }


    @DeleteMapping("/delete/{id}")
    public ResponseEntity<CategoryResponse> updateDeleteCategory(@PathVariable Long id) throws Exception {
        categoryService.DeleteCategory(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }


    @PutMapping("/update/{id}")
    public ResponseEntity<CategoryResponse> updateCategory(@PathVariable Long id, @RequestBody CategoryRequest categoryRequest)
            throws Exception
    {

        CategoryDto categoryDto = mapper.map(categoryRequest, CategoryDto.class);

        CategoryDto updatedCategory = categoryService.updateCategory(id, categoryDto);

        CategoryResponse categoryResponse = mapper.map(updatedCategory, CategoryResponse.class);
        return new ResponseEntity<CategoryResponse>(categoryResponse, HttpStatus.ACCEPTED);

    }
}
