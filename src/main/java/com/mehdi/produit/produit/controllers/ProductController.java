package com.mehdi.produit.produit.controllers;


import com.mehdi.produit.produit.dto.ProductDto;

import com.mehdi.produit.produit.requests.ProductRequest;
import com.mehdi.produit.produit.responses.CategoryResponse;
import com.mehdi.produit.produit.responses.ProductResponse;

import com.mehdi.produit.produit.services.ProductService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin("*")

@RequestMapping("/products")
public class ProductController {
    @Autowired
    private ProductService productService;

    @Autowired
    private ModelMapper mapper;

    @PostMapping()
    public ResponseEntity<ProductDto> createProduct(@RequestBody ProductRequest productRequest) {
        // Convert request to DTO
        ProductDto productDto = mapper.map(productRequest, ProductDto.class);
        productDto.setCategoryId(productRequest.getCategoryId());

        // Save product using ProductService
        ProductDto createdProductDto = productService.saveProduct(productDto);

        // Return the created product DTO with 201 Created status
        return ResponseEntity.status(HttpStatus.CREATED).body(createdProductDto);

    }

    @GetMapping("/getAll")
    public ResponseEntity<List<ProductResponse>> getAllProducts(@RequestParam(value="page", defaultValue = "1") int page,
                                                                 @RequestParam(value="limit", defaultValue = "6") int limit) {
        List<ProductDto> products = productService.getProducts(page, limit);
        List<ProductResponse> productResponses = products.stream()
                .map(product -> {
                    ProductResponse productResponse = mapper.map(product, ProductResponse.class);
                    Long categoryDto = product.getCategoryId();
                    productResponse.setCategoryId(categoryDto);
                    return productResponse;
                })
                .collect(Collectors.toList());
        return ResponseEntity.ok(productResponses);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProductResponse> getProduct(@PathVariable Long id) throws Exception {
        ProductDto productDto = productService.getProduct(id);
        ProductResponse productResponse = new ProductResponse();
        BeanUtils.copyProperties(productDto, productResponse);
        return new ResponseEntity<ProductResponse>(productResponse, HttpStatus.OK);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<ProductResponse> updateProduct(@PathVariable Long id,
                                                         @RequestBody ProductRequest productRequest) {
        ProductDto productDto = mapper.map(productRequest, ProductDto.class);
        productDto.setId(id);

        ProductDto updatedProductDto = productService.updateProduct(id, productDto);

        ProductResponse productResponse = mapper.map(updatedProductDto, ProductResponse.class);
        return ResponseEntity.ok(productResponse);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<CategoryResponse> updateDeleteCategory(@PathVariable Long id) throws Exception {
        productService.deleteProduct(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
