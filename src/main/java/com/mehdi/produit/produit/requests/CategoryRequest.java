package com.mehdi.produit.produit.requests;

import lombok.Data;

@Data
public class CategoryRequest {

    private String name;
    private String description;
    private String slug;
}
